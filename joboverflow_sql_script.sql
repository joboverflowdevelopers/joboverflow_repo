DROP TABLE IF EXISTS log_status;
DROP TABLE IF EXISTS account_type;
DROP TABLE IF EXISTS account_activity;
DROP TABLE IF EXISTS ratings;
DROP TABLE IF EXISTS job_post_status;
DROP TABLE IF EXISTS wallet;
DROP TABLE IF EXISTS skill;
DROP TABLE IF EXISTS user_type;
DROP TABLE IF EXISTS job_category;
DROP TABLE IF EXISTS image_path;
DROP TABLE IF EXISTS freelancer;
DROP TABLE IF EXISTS accomplishments;
DROP TABLE IF EXISTS hired_talents;
DROP TABLE IF EXISTS job_post;
DROP TABLE IF EXISTS employer;

CREATE TABLE log_status (
	log_status_id INTEGER NOT NULL AUTO_INCREMENT,
	_status varchar(10),
	PRIMARY KEY(log_status_id)
)ENGINE INNODB;
INSERT INTO log_status (_status) VALUES
('free');

CREATE TABLE account_type (
	account_type_id INTEGER NOT NULL AUTO_INCREMENT,
	_type varchar(10),
	PRIMARY KEY(account_type_id)
)ENGINE INNODB;
INSERT INTO account_type (_type) VALUES
('freelancer');

CREATE TABLE account_activity (
	account_activity_id INTEGER NOT NULL AUTO_INCREMENT,
	activity TINYINT,
	PRIMARY KEY(account_activity_id)
)ENGINE INNODB;
INSERT INTO account_activity (activity) VALUES
(1);

CREATE TABLE ratings (
	rating_id INTEGER NOT NULL AUTO_INCREMENT,
	rating INTEGER,
	PRIMARY KEY(rating_id)
)ENGINE INNODB;
INSERT INTO ratings (rating) VALUES
(10);

CREATE TABLE job_post_status (
	jps_id INTEGER NOT NULL AUTO_INCREMENT,
	_status TINYINT,
	PRIMARY KEY(jps_id)
)ENGINE INNODB;
INSERT INTO job_post_status (_status) VALUES
(1);

CREATE TABLE wallet (
	wallet_id INTEGER NOT NULL AUTO_INCREMENT,
	earnings varchar(255),
	e_money varchar(255),
	PRIMARY KEY(wallet_id)
)ENGINE INNODB;
INSERT INTO wallet (earnings, e_money) VALUES
('10000', '12000');

CREATE TABLE skill (
	skills_id INTEGER NOT NULL AUTO_INCREMENT,
	skill varchar(50),
	PRIMARY KEY(skills_id)
)ENGINE INNODB;
INSERT INTO skill (skill) VALUES
('JAVA');

CREATE TABLE user_type (
	user_type_id INTEGER NOT NULL AUTO_INCREMENT,
	_type TINYINT,
	PRIMARY KEY(user_type_id)
)ENGINE INNODB;
INSERT INTO user_type (_type) VALUES
(1);

CREATE TABLE job_category (
	job_category_id INTEGER NOT NULL AUTO_INCREMENT,
	_category varchar(50),
	PRIMARY KEY(job_category_id)
)ENGINE INNODB;
INSERT INTO job_category (_category) VALUES
('COMPUTER/IT');

CREATE TABLE image_path (
	image_path_id INTEGER NOT NULL AUTO_INCREMENT,
	_path varchar(255),
	PRIMARY KEY(image_path_id)
)ENGINE INNODB;
INSERT INTO image_path (_path) VALUES
('COMPUTER/IT');

CREATE TABLE freelancer (
	freelancer_id INTEGER NOT NULL AUTO_INCREMENT,
	_name varchar(20),
	email varchar(30),
	_password varchar(40),
	session_id varchar(32),
	_user_type_id INTEGER,
	_wallet_id INTEGER,
	_skills_id INTEGER,
	_rating_id INTEGER,
	_image_path_id INTEGER,
	_account_activity_id INTEGER,
	FOREIGN KEY (_user_type_id) REFERENCES user_type (user_type_id),
	FOREIGN KEY (_wallet_id) REFERENCES wallet (wallet_id),
	FOREIGN KEY (_skills_id) REFERENCES skill (skills_id),
	FOREIGN KEY (_rating_id) REFERENCES ratings (rating_id),
	FOREIGN KEY (_image_path_id) REFERENCES image_path (image_path_id),
	FOREIGN KEY (_account_activity_id) REFERENCES account_activity (account_activity_id),
	PRIMARY KEY(freelancer_id)
)ENGINE INNODB;
INSERT INTO freelancer (_name, email, _password, session_id) VALUES
('sample_name', 'email.@samplemail.com', '12345abcd', 'dn2x98xn1mlo3hb3dn2x98xn1mlo3hb3');

CREATE TABLE accomplishments (
	accomplishments_id INTEGER NOT NULL AUTO_INCREMENT,
	job_title varchar(30),
	_freelancer_id INTEGER,
	FOREIGN KEY (_freelancer_id) REFERENCES freelancer (freelancer_id),
	PRIMARY KEY(accomplishments_id)
)ENGINE INNODB;
INSERT INTO accomplishments (job_title) VALUES
('JAVA DEVELOPER');

CREATE TABLE hired_talents (
	hired_talents_id INTEGER NOT NULL AUTO_INCREMENT,
	talent_id INTEGER,
	FOREIGN KEY (talent_id) REFERENCES freelancer (freelancer_id),
	PRIMARY KEY(hired_talents_id)
)ENGINE INNODB;

CREATE TABLE job_post (
	job_post_id INTEGER NOT NULL AUTO_INCREMENT,
	category_id INTEGER,
	job_title varchar(50),
	duration INTEGER,
	required_applicants_needed INTEGER,
	project_budget varchar(255),
	_description varchar(150),
	job_post_status_id INTEGER,
	awarded_to INTEGER,
	FOREIGN KEY (category_id) REFERENCES job_category (job_category_id),
	FOREIGN KEY (job_post_status_id) REFERENCES job_post_status (jps_id),
	FOREIGN KEY (awarded_to) REFERENCES freelancer (freelancer_id),
	PRIMARY KEY(job_post_id)
)ENGINE INNODB;

CREATE TABLE employer (
	employer_id INTEGER NOT NULL AUTO_INCREMENT,
	_name varchar(20),
	email varchar(30),
	_password varchar(40),
	session_id varchar(32),
	about varchar(150),
	_user_type_id INTEGER,
	_wallet_id INTEGER,
	_rating_id INTEGER,
	_image_path_id INTEGER,
	_account_activity_id INTEGER,
	_hired_talents_id INTEGER,
	job_posts_id INTEGER,
	FOREIGN KEY (_user_type_id) REFERENCES user_type (user_type_id),
	FOREIGN KEY (_wallet_id) REFERENCES wallet (wallet_id),
	FOREIGN KEY (_rating_id) REFERENCES ratings (rating_id),
	FOREIGN KEY (_image_path_id) REFERENCES image_path (image_path_id),
	FOREIGN KEY (_account_activity_id) REFERENCES account_activity (account_activity_id),
	FOREIGN KEY (_hired_talents_id) REFERENCES hired_talents (hired_talents_id),
	FOREIGN KEY (job_posts_id) REFERENCES job_post (job_post_id),
	PRIMARY KEY(employer_id)
)ENGINE INNODB;
