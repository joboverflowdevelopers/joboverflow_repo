package com.joboverflowdevelopers;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

public class Logout extends Activity {

	Button logout;
	Dialog myLogout;
	Button no, yes;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_logout);
		logout = (Button)findViewById(R.id.logout);
		logout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				LogoutPopup();
			}
		});
	}

	public void LogoutPopup() {
		myLogout = new Dialog(Logout.this);
		myLogout.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myLogout.setContentView(R.layout.customlogout);
		myLogout.setTitle("Logout");
		no = (Button)myLogout.findViewById(R.id.no);
		yes = (Button)myLogout.findViewById(R.id.yes);
		yes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Toast.makeText(getApplicationContext(), "Logging out", Toast.LENGTH_LONG).show();
			}
		});

		no.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				myLogout.cancel();
			}
		});
		myLogout.show();
	}
}