package com.joboverflowdevelopers;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class MessageContent extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_messages_content);

		ImageView image = (ImageView)findViewById(R.id.messagesImage);
		TextView text = (TextView)findViewById(R.id.messagesText);

		text.setText(getIntent().getStringExtra("Employer"));
	}
}
