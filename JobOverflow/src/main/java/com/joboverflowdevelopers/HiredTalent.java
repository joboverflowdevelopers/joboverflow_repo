package com.joboverflowdevelopers;

public class HiredTalent {
	private String name;
	private String position;
	private String experience;
	private int photo;
	
	public HiredTalent (String name, String position, String experience, int photo) {
		this.name = name;
		this.position = position;
		this.experience = experience;
		this.photo = photo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public String getExperience() {
		return experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

	public int getPhoto() {
		return photo;
	}

	public void setPhoto(int photo) {
		this.photo = photo;
	}
}
