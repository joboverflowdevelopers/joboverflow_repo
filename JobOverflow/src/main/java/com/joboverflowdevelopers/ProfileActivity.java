package com.joboverflowdevelopers;

import android.app.Activity;
import android.widget.TextView;
import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.app.ActionBar;
import android.widget.Toast;
import android.support.v4.app.Fragment;
import android.app.Dialog;
import android.widget.Button;
import android.view.Window;
import android.content.Intent;
import android.widget.ImageView;
import android.graphics.Bitmap;

public class ProfileActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener
{
	private DrawerLayout drawer;
	private Button logout;
	private Dialog myLogout;
	private Button no, yes;
	private ImageView navImage;
	private static ProfileActivity _instance = null;
	NavigationView navigationView;

	public static ProfileActivity instance() {
		if(_instance == null) {
			_instance = new ProfileActivity();
		}
		return(_instance);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.profile_layout);
		Toolbar toolbar = (Toolbar)findViewById (R.id.toolbar);
		setSupportActionBar(toolbar);
		drawer = (DrawerLayout)findViewById (R.id.drawer_layout);
		ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, 
		toolbar, R.string.navigation_drawer_open,
			R.string.navigation_drawer_close);
		drawer.addDrawerListener(toggle);
		toggle.syncState();
		navigationView = (NavigationView) findViewById(R.id.nav_view);
		navigationView.setNavigationItemSelectedListener(this);
		if(savedInstanceState == null) {
			changeFragment(new JobVacanciesFragment());
		}
	}

	public void imageClick(View view) {
		changeFragment(new ProfileEmployer());
		drawer.closeDrawer(GravityCompat.START);
	}

	@Override
	public boolean onNavigationItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.nav_message:
				changeFragment(new MessagesFragment());
				Toast.makeText(this, "Messages", Toast.LENGTH_SHORT).show();
			break;
			case R.id.nav_eWallet:
				changeFragment(new EwalletFragment());
				Toast.makeText(this, "E-Wallet", Toast.LENGTH_SHORT).show();
			break;
			case R.id.nav_jobVecancies:
				Toast.makeText(this, "Job Vecancies", Toast.LENGTH_SHORT).show();
			break;
			case R.id.nav_jobPost:
				changeFragment(new JobPostFragment());
				Toast.makeText(this, "Job Post", Toast.LENGTH_SHORT).show();
			break;
			case R.id.nav_hiredTalents:
				changeFragment(new HiredTalentsFragment());
				Toast.makeText(this, "Hired Talents", Toast.LENGTH_SHORT).show();
			break;
			case R.id.nav_Transaction:
				changeFragment(TransactionFragment.instance());
				Toast.makeText(this, "Transaction history", Toast.LENGTH_SHORT).show();
			break;
			case R.id.nav_logOut:
				LogoutPopup();
			break;
		}
		drawer.closeDrawer(GravityCompat.START);
		return(true);
	}

	public void changeFragment(Fragment fragment) {
		getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, 
		fragment).commit();
	}

	@Override
	public void onBackPressed() {
		if(drawer.isDrawerOpen(GravityCompat.START)) {
			drawer.closeDrawer(GravityCompat.START);
		}
		else {
			super.onBackPressed();
		}
	}

	public void changePicture(View view)
	{
		Toast.makeText(this, "Edit Image", Toast.LENGTH_SHORT).show();
	}

	public void LogoutPopup() {
		final SessionManager sm = new SessionManager(this, "login_status");
		myLogout = new Dialog(ProfileActivity.this);
		myLogout.requestWindowFeature(Window.FEATURE_NO_TITLE);
		myLogout.setContentView(R.layout.customlogout);
		myLogout.setTitle("Logout");
		no = (Button)myLogout.findViewById(R.id.no);
		yes = (Button)myLogout.findViewById(R.id.yes);
		yes.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				sm.save("status", false);
				Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);
				startActivity(intent);
			}
		});
		no.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				myLogout.cancel();
			}
		});
		myLogout.show();
	}
}
