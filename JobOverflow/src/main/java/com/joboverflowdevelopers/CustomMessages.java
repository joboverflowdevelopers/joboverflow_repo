package com.joboverflowdevelopers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

public class CustomMessages extends RecyclerView.Adapter<CustomMessages.MessagesHolder> {
	private List<Conversation> messagesList;
	private Context context;

	public CustomMessages(List<Conversation> messagesList, Context context) {
		this.messagesList = messagesList;
		this.context = context;
	}


	public CustomMessages(ArrayList<Conversation> messagesList) {
		this.messagesList = messagesList;
		messagesList = new ArrayList<>(messagesList);
	}

	class MessagesHolder extends RecyclerView.ViewHolder {
		ImageView m_image;
		TextView m_name;
		TextView m_msgOverview;
		TextView m_date;
		View mv;
		public MessagesHolder(View msgView) {
			super(msgView);
			mv = msgView;
			m_image=(ImageView) msgView.findViewById(R.id.image);
			m_name = (TextView) msgView.findViewById(R.id.name);
			m_msgOverview = (TextView) msgView.findViewById(R.id.msgOverview);
			m_date = (TextView) msgView.findViewById(R.id.date);
		}
	}

	@Override
	public MessagesHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View layout= LayoutInflater.from(parent.getContext()).inflate(R.layout.messages, parent,false);
		return(new MessagesHolder(layout));
	}

	@Override
	public void onBindViewHolder(MessagesHolder holder, final int position) {
		holder.m_name.setText(messagesList.get(position).getName());
		holder.m_msgOverview.setText(messagesList.get(position).getOverview());
		holder.m_date.setText(messagesList.get(position).getDate());
		holder.m_image.setImageResource(messagesList.get(position).getPhoto());
		holder.mv.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View view) {
				Toast.makeText(context, "Conversation with: " + messagesList.get(position).getName(), Toast.LENGTH_LONG).show();
			}
		});
	}

	@Override
	public int getItemCount() {
		return(messagesList.size());
	}

	public void setfilter(List<Conversation> cn)
	{
		messagesList=new ArrayList<>();
		messagesList.addAll(cn);
		notifyDataSetChanged();
	}
}

// Note: Create your own xml files and follow the id naming convention