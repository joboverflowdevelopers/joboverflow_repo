package com.joboverflowdevelopers;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import android.widget.Button;
import android.widget.ImageView;
import android.net.Uri;
import android.provider.MediaStore.Images.Media;
import android.content.Intent;
import android.provider.MediaStore;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.MenuItem;
import android.content.Context;
import android.widget.LinearLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.widget.TextView;
import android.widget.RatingBar;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;

public class ProfileEmployer extends Fragment {
	private final int PICK_CAMERA = 1;
	private final int PICK_IMAGE = 0;
	private LinearLayout camera;
	private LinearLayout gallery;
	private LinearLayout bottomSheet;
	private Button changePicture;
	private View view;
	private View layout;
	private Uri imageUri;
	private ImageView imageView;
	private Context context;
	private BottomSheetBehavior bottomBehavior;
	private static ProfileEmployer _instance = null;
	private RatingBar ratingBar;
	private Button goodBtn;
	private Button badBtn;
	private int count = 0;
	private float rating = 0;
	private float ave;
	private ExpandableListView expandableListView;
	private ExpandableListAdapter adapter;
	private List<String> expandableListTitle;
	private HashMap<String, List<String>> expandableChild;
	private ProfileActivity profileActivity;

	public static ProfileEmployer instance() {
		if(_instance == null) {
			_instance = new ProfileEmployer();
		}
		return(_instance);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.employer_fragment, container, false);
		profileActivity = ProfileActivity.instance();
		context = getActivity();
		changePicture = (Button) view.findViewById(R.id.changePicture);
		imageView = (ImageView) view.findViewById(R.id.imageView);
		camera = (LinearLayout) view.findViewById(R.id.nav_camera);
		gallery = (LinearLayout) view.findViewById(R.id.nav_gallery);
		bottomSheet =(LinearLayout) view.findViewById(R.id.bottomSheet);
		bottomBehavior = BottomSheetBehavior.from(bottomSheet);
		ratingBar = (RatingBar) view.findViewById(R.id.ratingBar);
		gallery = (LinearLayout) view.findViewById(R.id.nav_gallery);
		ratingBar.setNumStars(5);
		expandableListView = (ExpandableListView) view.findViewById(R.id.expandableListView);
		expandableChild = ExpandableList.getData();
		expandableListTitle  = new ArrayList<String> (expandableChild.keySet());
		adapter = new ExpandableListAdapterCustom(context, expandableListTitle, expandableChild);
		expandableListView.setAdapter(adapter);
		changePicture.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if(bottomBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED) {
					isExpand(true);
				}
				else {
					isExpand(false);
				}
			}
		});
		camera.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent camera = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				startActivityForResult(camera, PICK_CAMERA);
				Toast.makeText(context, "Camera", Toast.LENGTH_SHORT).show();
				isExpand(false);
			}
		});
		gallery.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
				gallery.setType("image/*");
				startActivityForResult(gallery, PICK_IMAGE);
				Toast.makeText(context, "Gallery", Toast.LENGTH_SHORT).show();
				isExpand(false);
			}
		});
		return(view);
	}

	public void totalAverage(float rating) {
		ave = (float)rating / (float)++count;
		Toast.makeText(context, "Ave:" + ave, Toast.LENGTH_SHORT).show();
		ratingBar.setRating((float)ave);
	}

	public void isExpand(boolean isCheck) {
		if(isCheck) {
			bottomBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
		}
		else {
			bottomBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == getActivity().RESULT_OK) {
			switch(requestCode) {
				case PICK_CAMERA:
					Bundle bundle = data.getExtras();
					final Bitmap bmp = (Bitmap) bundle.get("data");
					imageView.setImageBitmap(bmp);
					break;
				case PICK_IMAGE:
					Uri image = data.getData();
					imageView.setImageURI(image);
					break;
				default:
					break;
			}
		}
	}
}
