package com.joboverflowdevelopers;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Filterable;
import android.widget.Filter;
import java.util.ArrayList;

public class TalentsAdapter extends RecyclerView.Adapter<TalentsAdapter.TalentsViewHolder> implements Filterable {
	private static ArrayList<Talents> mTalentsList;
	private ArrayList<Talents> mTalentsListFull;
	private OnItemClickListener listener;
	public interface OnItemClickListener {
		void onItemClick(int position);
	};

	public void setOnItemClickListener(OnItemClickListener l) {
		listener = l;
	};

	public static class TalentsViewHolder extends RecyclerView.ViewHolder {
		public ImageView mImageView;
		public TextView mTextView1;
		public TextView mTextView2;
		public TalentsViewHolder(View talentsView, final OnItemClickListener listen) {
			super(talentsView);
			mImageView = talentsView.findViewById(R.id.imageView);
			mTextView1 = talentsView.findViewById(R.id.textView);
			mTextView2 = talentsView.findViewById(R.id.textView2);

			talentsView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if(listen != null) {
						int position = getAdapterPosition();
						if(position != RecyclerView.NO_POSITION) {
							listen.onItemClick(position);
						}
					}
				}
			});
		}
	}

	public TalentsAdapter(ArrayList<Talents> j) {
		mTalentsList = j;
		mTalentsListFull = new ArrayList<>(mTalentsList);
	}

	@Override
	public TalentsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.talentlist, parent, false);
		TalentsViewHolder jvh = new TalentsViewHolder(v, listener);
		return jvh;
	}

	@Override
	public void onBindViewHolder(TalentsViewHolder holder, int position) {
		Talents currentTalents = mTalentsList.get(position);
		holder.mImageView.setImageResource(currentTalents.getImageResource());
		holder.mTextView1.setText(currentTalents.getText1());
		holder.mTextView2.setText(currentTalents.getText2());
	}

	@Override
	public int getItemCount() {
		return mTalentsList.size();
	}

	@Override
	public Filter getFilter() {
		return TalentsFilter;
	}

	private Filter TalentsFilter = new Filter() {
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			ArrayList<Talents> filteredList = new ArrayList<>();
			String filterPattern;
			if(constraint == null || constraint.length() == 0) {
				filteredList.addAll(mTalentsListFull);
			}
			else {
				filterPattern = constraint.toString().toLowerCase().trim();
				for(Talents talents : mTalentsListFull) {
					if(talents.getText1().toLowerCase().contains(filterPattern)) {
						filteredList.add(talents);
					}
				}
			}
			FilterResults results = new FilterResults();
			results.values = filteredList;
			return results;
		}

		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			mTalentsList.clear();
			mTalentsList.addAll((ArrayList)results.values);
			notifyDataSetChanged();
		}
	};
}
