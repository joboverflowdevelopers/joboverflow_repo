package com.joboverflowdevelopers;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.Intent;

public class MainActivity extends AppCompatActivity {

	Intent intent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		final SessionManager sm = new SessionManager(this, "login_status");
		Thread splash = new Thread()
		{
			public void run()
			{
				try{
					sleep(2000);
				}
				catch (Exception e)
				{
					System.out.println("Error loading splash");
				}
				finally {
						System.out.println(sm.getBooleanValue("status"));
					if(sm.getBooleanValue("status")) {
						intent = new Intent(MainActivity.this, ProfileActivity.class);
						startActivity(intent);
					}
					else {
						intent = new Intent(MainActivity.this, LoginActivity.class);
						startActivity(intent);
					}
				}
			}
		};
		splash.start();
	}

	@Override
	protected void onPause() {
		super.onPause();
		finish();
	}
}
