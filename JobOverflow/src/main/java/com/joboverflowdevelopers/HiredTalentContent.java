package com.joboverflowdevelopers;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class HiredTalentContent extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.hired_talents_content);
		ImageView image = (ImageView)findViewById(R.id.transactionImage);
		TextView text = (TextView)findViewById(R.id.contentText);
		text.setText(getIntent().getStringExtra("Employer"));
	}
}
