package com.joboverflowdevelopers;

import android.app.Activity;
import android.widget.TextView;
import android.os.Bundle;
import android.view.View;
import android.view.MenuItem;
import android.app.ActionBar;
import android.widget.Toast;
import android.widget.ImageView;
import android.widget.EditText;
import android.widget.Button;
import android.widget.RadioButton;
import android.view.View;
import android.content.Intent;
import android.widget.RadioGroup;
import android.text.TextWatcher;
import android.text.Editable;
import java.util.ArrayList;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.AuthResult;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;

public class RegisterActivity extends Activity
{
	private TextView errorMessage;
	private ImageView logo;
	private EditText emailField;
	private EditText passwordField;
	private EditText confirm_passwordField;
	private RadioButton userType;
	private Button submitButton;
	private ArrayList<EditText> fields = null;
	private ArrayList<TextView> errorMessages = null;
	private boolean nameAndAllFieldsHasInput = false;
	private boolean emailAndAllFieldsHasInput = false;
	private boolean passwordAndAllFieldsHasInput = false;
	private boolean confpasswordAndAllFieldsHasInput = false;
	private FirebaseAuth firebaseAuth;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.register_layout);
		firebaseAuth = FirebaseAuth.getInstance();
		fields = new ArrayList<EditText>();
		errorMessages = new ArrayList<TextView>();
		initWidgets();
		submitButton.setEnabled(true);
		submitButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if(InputValidator.validateRegistration(fields, errorMessages)) {
					String email = emailField.getText().toString().trim();
					String password = passwordField.getText().toString().trim();
					System.out.println(email);
					System.out.println(password);
					firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
						@Override
						public void onComplete(Task<AuthResult> task) {
							Toast.makeText(getApplicationContext(), "Successful Registration", Toast.LENGTH_LONG).show();
						}
					});
				}
			}
		});
	}

	public void initWidgets() {
		logo = (ImageView)findViewById(R.id.logo);
		emailField = (EditText)findViewById(R.id.email);
		passwordField = (EditText)findViewById(R.id.password);
		confirm_passwordField = (EditText)findViewById(R.id.confirm_password);
		errorMessage = (TextView)findViewById(R.id.errorMessage);
		submitButton = (Button)findViewById(R.id.submit);
		fields.add(emailField);
		fields.add(passwordField);
		fields.add(confirm_passwordField);
		errorMessages.add(errorMessage);
	}

	public void openTermOfUse(View v) {
		Intent intent = new Intent(RegisterActivity.this, PrivatePolicy.class);
		startActivity(intent);
	}

	public void privacy(View v) {
		Intent intent = new Intent(RegisterActivity.this, PrivatePolicy.class);
		startActivity(intent);
	}

	private TextWatcher	textWatcher = new TextWatcher() {
		@Override
		public void beforeTextChanged(CharSequence s, int start, int count, int after) {
		}

		@Override
		public void afterTextChanged(Editable s) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			emailField.addTextChangedListener(textWatcher);
			passwordField.addTextChangedListener(textWatcher);
			confirm_passwordField.addTextChangedListener(textWatcher);
			 if(emailField.getText().hashCode() == s.hashCode()) {
				if(confpasswordAndAllFieldsHasInput) {
					submitButton.setEnabled(true);
				}
			}
			else if(passwordField.getText().hashCode() == s.hashCode()) {
				if(confpasswordAndAllFieldsHasInput) {
					submitButton.setEnabled(true);
				}
			}
			else if(confirm_passwordField.getText().hashCode() == s.hashCode()) {
				if(confpasswordAndAllFieldsHasInput) {
					submitButton.setEnabled(true);
				}
			}
		}
	};
}
