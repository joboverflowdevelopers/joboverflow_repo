package com.joboverflowdevelopers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

public class JobAdapter extends RecyclerView.Adapter<JobAdapter.HolderView> {
	private List<Job> joblist;
	private Context context;
	public JobAdapter(List<Job> joblist, Context context) {
		this.joblist = joblist;
		this.context = context;
	}

	class HolderView extends RecyclerView.ViewHolder {
		ImageView v_image;
		TextView v_name;
		TextView v_description;
		TextView v_price;
		View jv;
		public HolderView(View jobView) {
			super(jobView);
			jv = jobView;
			v_image=(ImageView) jobView.findViewById(R.id.job_image);
			v_name = (TextView) jobView.findViewById(R.id.job_title);
			v_description = (TextView) jobView.findViewById(R.id.job_description);
			v_price = (TextView) jobView.findViewById(R.id.job_price);
		}
	}

	@Override
	public HolderView onCreateViewHolder(ViewGroup parent, int viewType) {
		View layout= LayoutInflater.from(parent.getContext()).inflate(R.layout.joblist, parent,false);
		return new HolderView(layout);
	}

	@Override
	public void onBindViewHolder(HolderView holder, final int position) {
		holder.v_name.setText(joblist.get(position).getName());
		holder.v_description.setText(joblist.get(position).getDescription());
		holder.v_price.setText(joblist.get(position).getPrice());
		holder.v_image.setImageResource(joblist.get(position).getPhoto());
		holder.jv.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View view) {
				Toast.makeText(context, "You applied in " + joblist.get(position).getName(), Toast.LENGTH_LONG).show();
			}
		});
	}

	@Override
	public int getItemCount() {
		return joblist.size();
	}

	public void setfilter(List<Job> listjob)
	{
		joblist=new ArrayList<>();
		joblist.addAll(listjob);
		notifyDataSetChanged();
	}
}
