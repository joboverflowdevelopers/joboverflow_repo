package com.joboverflowdevelopers;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

public class HiredTalentsAdapter extends RecyclerView.Adapter<HiredTalentsAdapter.HolderView> {
	private List<HiredTalent> hired_talents;
	private Context context;

	public HiredTalentsAdapter(List<HiredTalent> hired_talents, Context context) {
		this.hired_talents = hired_talents;
		this.context = context;
	}

	class HolderView extends RecyclerView.ViewHolder {
		ImageView h_image;
		TextView h_name;
		TextView h_description;
		TextView h_price;
		View ht;
		public HolderView(View HiredTalent) {
			super(HiredTalent);
			ht = HiredTalent;
			h_image = (ImageView) HiredTalent.findViewById(R.id.hired_image);
			h_name = (TextView) HiredTalent.findViewById(R.id.hired_title);
			h_description = (TextView) HiredTalent.findViewById(R.id.hired_description);
			h_price = (TextView) HiredTalent.findViewById(R.id.hired_price);
		}
	}

	@Override
	public HolderView onCreateViewHolder(ViewGroup parent, int viewType) {
		View layout= LayoutInflater.from(parent.getContext()).inflate(R.layout.hired_talents, parent,false);
		return new HolderView(layout);
	}

	@Override
	public void onBindViewHolder(HolderView holder, final int position) {
		holder.h_name.setText(hired_talents.get(position).getName());
		holder.h_description.setText(hired_talents.get(position).getPosition());
		holder.h_price.setText(hired_talents.get(position).getExperience());
		holder.h_image.setImageResource(hired_talents.get(position).getPhoto());
		holder.ht.setOnClickListener(new View.OnClickListener() {
			@Override public void onClick(View view) {
				Toast.makeText(context, "You applied in " + hired_talents.get(position).getName(), Toast.LENGTH_LONG).show();
			}
		});
	}

	@Override
	public int getItemCount() {
		return hired_talents.size();
	}

	public void setfilter(List<HiredTalent> ht)
	{
		hired_talents=new ArrayList<>();
		hired_talents.addAll(ht);
		notifyDataSetChanged();
	}
}
