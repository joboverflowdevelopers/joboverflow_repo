package com.joboverflowdevelopers;

public class Conversation {
	private String name;
	private String messageOverview;
	private String date;
	private int photo;
	
	public Conversation(String name, String messageOverview, String date, int photo) {
		this.name = name;
		this.messageOverview = messageOverview;
		this.date = date;
		this.photo = photo;
	}

	public String getName() {
		return(name);
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getOverview() {
		return(messageOverview);
	}

	public void setOverview(String messageOverview) {
		this.messageOverview = messageOverview;
	}

	public String getDate() {
		return(date);
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getPhoto() {
		return photo;
	}

	public void setPhoto(int photo) {
		this.photo = photo;
	}
}
