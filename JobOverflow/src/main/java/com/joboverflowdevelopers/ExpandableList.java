package com.joboverflowdevelopers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExpandableList
{
	public static HashMap<String, List<String>> getData() {
		HashMap<String, List<String>> expandableList = new HashMap<String, List<String>>();
		List<String> skills = new ArrayList<String>();
		List<String> accomplishments = new ArrayList<String>();
		skills.add("Python");
		skills.add("Java");
		skills.add("C++");
		skills.add("C");
		accomplishments.add("Python Programmer");
		accomplishments.add("Java Programmer");
		accomplishments.add("C++ Programmer");
		accomplishments.add("C Programmer");
		expandableList.put("SKILLS", skills);
		expandableList.put("ACCOMPLISHMENTS", accomplishments);
		return(expandableList);
	}
}
