package com.joboverflowdevelopers;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class TransactionContent extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_transaction_content);

		ImageView image = (ImageView)findViewById(R.id.transactionImage);
		TextView text = (TextView)findViewById(R.id.contentText);

		text.setText(getIntent().getStringExtra("Employer"));
	}
}
