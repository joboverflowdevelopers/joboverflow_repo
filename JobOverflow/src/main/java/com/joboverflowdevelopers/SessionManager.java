package com.joboverflowdevelopers;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.content.SharedPreferences.Editor;
import android.content.Context;

public class SessionManager
{
	private Context thisCtx;
	private SharedPreferences pref;
	private SharedPreferences.Editor editor;

	public SessionManager(Context ctx, String preferenceName) {
		thisCtx = ctx;
		pref = PreferenceManager.getDefaultSharedPreferences(thisCtx);
		editor = pref.edit();
	}

	public void save(String key, String value) {
		editor.putString(key, value);
		editor.apply();
	}

	public void save(String key, boolean value) {
		editor.putBoolean(key, value);
		editor.apply();
	}

	public Boolean getBooleanValue(String key) {
		boolean res = false;
		if(pref.contains(key)) {
			res = pref.getBoolean(key, res);
		}
		return(res);
	}

	public String getStringValue(String key) {
		String res = null;
		if(pref.contains(key)) {
			res = pref.getString(key, res);
		}
		return(res);
	}
}
