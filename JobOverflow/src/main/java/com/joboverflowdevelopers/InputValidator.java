package com.joboverflowdevelopers;

import java.util.ArrayList;
import android.widget.TextView;
import android.widget.EditText;
import android.view.View;
import android.graphics.Color;

public class InputValidator
{
	public static String username = "admin@jo.com";
	public static String password = "admin123";

	public static boolean isEmpty(EditText ed) {
		boolean res = false;
		String value = ed.getText().toString();
		if(value == null || value.length() == 0) {
			res = true;
		}
		return(res);
	}

	public static boolean isEmpty(ArrayList<EditText> fields) {
		boolean res = false;
		String value = null;
		for(EditText ed : fields) {
			value = ed.getText().toString();
			if(value == null || value.length() == 0) {
				res = true;
			}
		}
		return(res);
	} 

	public static boolean isEmail(EditText ed) {
		boolean res = false;
		String email = ed.getText().toString();
		boolean hasDot = email.contains(".");
		boolean hasAt = email.contains("@");
		if(hasDot == true && hasAt == true) {
			String afterAtString = email.split("@")[1];
			boolean hasDotAfterAt = afterAtString.contains(".");
			res = (hasDotAfterAt == true) ? true : false;
		}
		else {
			res = false;
		}
		return(res);
	}

	public static boolean validateLogin(ArrayList<EditText> fields, ArrayList<TextView> errorMessage) {
		boolean res = false;
		EditText email = fields.get(0);
		EditText password = fields.get(1);
		if(!isEmpty(email) && !isEmpty(password)) {
			if(isEmail(email)) {
				if(isMatch(email, password)) {
					return(true);
				}
				else {
					errorMessage.get(0).setTextColor(Color.parseColor("#b34104"));
					errorMessage.get(0).setText("Wrong email and/or password");
					return(res);
				}
			}
			else {
				errorMessage.get(0).setText("");
				email.setError("invalid email format");
				return(res);
			}
		}
		else if(isEmpty(email) && !isEmpty(password)) {
			errorMessage.get(0).setText("");
			email.setError("required field");
			return(res);
		}
		else if(!isEmpty(email) && isEmpty(password)) {
			errorMessage.get(0).setText("");
			password.setError("required field");
			return(res);
		}
		else {
			for(EditText ed : fields) {
				ed.setError("required field");
			}
			return(res);
		}
	}

	public static boolean validateRegistration(ArrayList<EditText> fields, ArrayList<TextView> errorMessage) {
		boolean res = false;
		EditText email = fields.get(0);
		EditText password = fields.get(1);
		EditText confirm_password = fields.get(2);
		if(!isEmpty(fields)) {
			if(isEmail(email)) {
				if(isMatch(password, confirm_password)) {
					return(true);
				}
				else {
					errorMessage.get(0).setTextColor(Color.parseColor("#b34104"));
					errorMessage.get(0).setText("passwords does not match");
					return(res);
				}
			}
			else {
				errorMessage.get(0).setText("");
				email.setError("invalid email format");
				return(res);
			}
		}
		else if(isEmpty(email) && !isEmpty(password) && !isEmpty(confirm_password)) {
			errorMessage.get(0).setText("");
			email.setError("required field");
			return(res);
		}
		else if(!isEmpty(email) && isEmpty(password) && !isEmpty(confirm_password)) {
			errorMessage.get(0).setText("");
			password.setError("required field");
			return(res);
		}
		else if(!isEmpty(email) && !isEmpty(password) && isEmpty(confirm_password)) {
			errorMessage.get(0).setText("");
			confirm_password.setError("required field");
			return(res);
		}
		else {
			for(EditText ed : fields) {
				ed.setError("required field");
			}
			return(res);
		}
	}

	public static boolean isMatch(EditText email, EditText pass) {
		boolean res = false;
		String e = email.getText().toString().trim();
		String p = pass.getText().toString().trim();
		if(e.equals(username) && p.equals(password)) {
			res = true;
		}
		return(res);
	}
}
