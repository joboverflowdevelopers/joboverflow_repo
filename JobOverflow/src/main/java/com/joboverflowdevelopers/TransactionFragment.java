package com.joboverflowdevelopers;

import android.os.Bundle;
import java.util.ArrayList;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.widget.SearchView;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.content.Context;
import android.app.Activity;
import android.widget.Toast;
import android.view.inputmethod.EditorInfo;

public class TransactionFragment extends Fragment {
	View view;
	Context content;
	private RecyclerView tRecyclerView;
	private CustomAdapter cAdapter;
	private RecyclerView.LayoutManager mLayoutManager;
	ArrayList<Transactions> aTransactions;
	private static TransactionFragment _instance = null;

	public static TransactionFragment instance() {
		if(_instance == null) {
			_instance = new TransactionFragment();
		}
		return(_instance);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.activity_transaction, container, false);
		content = getActivity();
		transactionList();
		setUpRecyclerView();
		return(view);
	}

	private void transactionList() {
		aTransactions = new ArrayList<>();
		aTransactions.add(new Transactions((R.drawable.tao1), "Edward Isles", "Status: Paid", "PHP 30, 000"));
		aTransactions.add(new Transactions((R.drawable.tao2), "Louievic Sancon", "Status: Paid", "PHP 30, 000"));
		aTransactions.add(new Transactions((R.drawable.tao3), "Heji Palanski", "Status: Paid", "PHP 30, 000"));
		aTransactions.add(new Transactions((R.drawable.tao4), "Bernie Baltazar", "Status: Paid", "PHP 30, 000"));
		aTransactions.add(new Transactions((R.drawable.tao5), "Patrick Obra", "Status: Paid", "PHP 30, 000"));
	}

	private void setUpRecyclerView() {
		tRecyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
		tRecyclerView.setHasFixedSize(true);
		mLayoutManager = new LinearLayoutManager(content);
		cAdapter = new CustomAdapter(aTransactions);
		tRecyclerView.setLayoutManager(mLayoutManager);
		tRecyclerView.setAdapter(cAdapter);
		cAdapter.setOnItemClickListener(new CustomAdapter.OnItemClickListener() {
			@Override
			public void onItemClick(int position) {
				String str = "You selected: " + aTransactions.get(position).mText1;
				Toast.makeText(content, str, Toast.LENGTH_SHORT).show();
			}
		});
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getActivity().getMenuInflater();
		inflater.inflate(R.menu.search_menu, menu);
		MenuItem searchItem = menu.findItem(R.id.search);
		SearchView searchView = (SearchView) searchItem.getActionView();
		searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener(){
			@Override
			public boolean onQueryTextSubmit(String query) {
				return false;
			}

			@Override
			public boolean onQueryTextChange(String newText) {
				cAdapter.getFilter().filter(newText);
				return false;
			}
		});
		return true;
	}
}
