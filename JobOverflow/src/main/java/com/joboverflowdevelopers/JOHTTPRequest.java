package com.joboverflowdevelopers;

import java.net.HttpURLConnection;
import java.net.URL;
import java.lang.StringBuilder;
import java.io.OutputStream;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URLEncoder;

public class JOHTTPRequest
{
	private static final String REGISTER_URL = "https://192.168.43.23:3434/register";
	private static final String LOGIN_URL = "https://192.168.43.23:3434/login";

	public static String post(String reqBody) {
		StringBuilder sb = new StringBuilder("");
		try {
			URL url = new URL(REGISTER_URL);
			HttpURLConnection uc = (HttpURLConnection)url.openConnection();
			uc.setDoOutput(true);
			uc.setRequestProperty("Content-Type", "application/json");
			uc.setRequestProperty("Accept", "application/json");
			OutputStream out = uc.getOutputStream();
			out.write(reqBody.getBytes());
			InputStream is = uc.getInputStream();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			String line = "";
			while((line = br.readLine()) != null) {
				sb.append(line);
			}
			uc.disconnect();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return(sb.toString());
	}
}