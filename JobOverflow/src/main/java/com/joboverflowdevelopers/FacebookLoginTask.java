package com.joboverflowdevelopers;

import android.os.AsyncTask;
import java.net.HttpURLConnection;
import java.net.URL;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.io.OutputStream;
import org.json.JSONObject;
import java.io.DataOutputStream;
import java.io.OutputStreamWriter;
import java.lang.StringBuilder;
import java.io.BufferedReader;

public class FacebookLoginTask extends AsyncTask<String, String, String>
{
	private JSONObject jo =  null;

	public FacebookLoginTask() {
	}

	public String doInBackground(String... jo) {
		StringBuilder sb = new StringBuilder("");
		try {
			URL url = new URL("https://localhost:3434/register");
			HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
			urlConnection.setRequestMethod("POST");
			urlConnection.setRequestProperty("Content-Type", "application/json");
			urlConnection.setRequestProperty("Accept", "application/json");
			DataOutputStream out = new DataOutputStream(urlConnection.getOutputStream());
			out.write(jo.toString().getBytes());
			InputStream is = urlConnection.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String line = "";
			while((line = br.readLine()) != null) {
				sb.append(line);
			}
			urlConnection.disconnect();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return(sb.toString());
	}
}
