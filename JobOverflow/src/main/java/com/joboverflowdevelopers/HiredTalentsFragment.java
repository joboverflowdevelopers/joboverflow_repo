package com.joboverflowdevelopers;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.content.Intent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import android.support.v4.app.Fragment;
import android.view.MenuInflater;
import android.content.Context;

public class HiredTalentsFragment extends Fragment {
	private View view;
	private Context ctx;
	private SearchView searchView;
	private RecyclerView listHiredTalents;
	private List<HiredTalent> hiredTalent;
	private HiredTalentsAdapter adapter;
	private static HiredTalentsFragment _instance = null;

	public static HiredTalentsFragment instance() {
		if(_instance == null) {
			_instance = new HiredTalentsFragment();
		}
		return(_instance);
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.job_vacancies, container, false);
		hiredTalent = new ArrayList<>();
		ctx = getActivity();
		setHasOptionsMenu(true);
		hiredTalent.add(new HiredTalent("Edward Isles", "MEAN Stack Developer", "4 Years Experience", R.drawable.tao1));
		hiredTalent.add(new HiredTalent("Louievic Sancon", "Full-Stack Java Developer", "5 Years Experience", R.drawable.tao2));
		hiredTalent.add(new HiredTalent("Heji Palanski", "Full-Stack C Developer", "10 Years Experience", R.drawable.tao3));
		hiredTalent.add(new HiredTalent("Bernie Baltazar", "Junior Front-End Developer", "1 Years Experience", R.drawable.tao4));
		hiredTalent.add(new HiredTalent("Patrick Obra","Senior C# Developer", "6 Years Experience", R.drawable.tao5));
		listHiredTalents = (RecyclerView) view.findViewById(R.id.listshow);
		listHiredTalents.setHasFixedSize(true);
		LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ctx);
		listHiredTalents.setLayoutManager(linearLayoutManager);
		adapter = new HiredTalentsAdapter(hiredTalent, ctx);
		listHiredTalents.setAdapter(adapter);
		return view;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.search_menu, menu);
		final MenuItem myActionMenuJob = menu.findItem(R.id.search);
		searchView = (SearchView) myActionMenuJob.getActionView();
		((EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text)).setHintTextColor(getResources().getColor(R.color.white));
		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String query) {
				if (!searchView.isIconified()) {
					searchView.setIconified(true);
				}
				myActionMenuJob.collapseActionView();
				return false;
			}

			@Override
			public boolean onQueryTextChange(String newText) {
				final  List<HiredTalent> filterJobList = filter(hiredTalent,newText);
				adapter.setfilter(filterJobList);
				return true;
			}
		});
	}

	private List<HiredTalent> filter(List<HiredTalent> jl, String query)
	{
		query=query.toLowerCase();
		final List<HiredTalent> filteredJobList=new ArrayList<>();
		for(HiredTalent jb:jl)
		{
			final String text=jb.getName().toLowerCase();
			if (text.contains(query))
			{
				filteredJobList.add(jb);
			}
		}
		return filteredJobList;
	}
}
