package com.joboverflowdevelopers;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.view.View;
import android.graphics.Color;
import android.text.TextWatcher;
import android.view.View.OnTouchListener;
import android.view.MotionEvent;
import android.text.Editable;
import android.widget.ImageView;
import com.facebook.FacebookSdk;
import com.facebook.HttpMethod;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.login.LoginResult;
import com.facebook.FacebookException;
import com.facebook.login.widget.LoginButton;
import android.content.Intent;
import org.json.JSONObject;
import java.util.Arrays;
import java.util.ArrayList;

public class LoginActivity extends Activity
{
	private static final String EMAIL = "email";
	private ImageView iv;
	private TextView errorMessage;
	private EditText email;
	private EditText password;
	private TextView signup;
	private LoginButton loginButton;
	private Button joButton;
	private CallbackManager callbackManager;
	private FacebookLoginTask fbtask = new FacebookLoginTask();
	private int countNotEmptyFileds = 0;
	private ArrayList<EditText> fields = null;
	private	ArrayList<TextView> errorMessages = null;
	private Intent intent;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		System.out.println("ONCREATE");
		super.onCreate(savedInstanceState);
		FacebookSdk.sdkInitialize(getApplicationContext());
		setContentView(R.layout.login_layout);
		fields = new ArrayList<EditText>();
		errorMessages = new ArrayList<TextView>();
		initWidgets();
		email.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {
			}

			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before, int count) {
				errorMessage.setText("");
				if(!InputValidator.isEmpty(password)) {
					enableLogin();
				}
			}
		});

		password.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {}

			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

			public void onTextChanged(CharSequence s, int start, int before, int count) {
				errorMessage.setText("");
				if(!InputValidator.isEmpty(email)) {
					enableLogin();
				}
			}
		});
		signup = (TextView)findViewById(R.id.signup);
		final SessionManager sm = new SessionManager(this, "login_status");
		joButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if(InputValidator.validateLogin(fields, errorMessages)) {
					sm.save("status", true);
					intent = new Intent(LoginActivity.this, ProfileActivity.class);
					startActivity(intent);
				}
			}
		});
		callbackManager = CallbackManager.Factory.create();
		loginButton = (LoginButton) findViewById(R.id.login_button);
		loginButton.setReadPermissions(Arrays.asList(EMAIL));
		loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
			@Override
			public void onSuccess(LoginResult loginResult) {
				System.out.println("AccessToken" + loginResult.getAccessToken());
				AccessToken token = loginResult.getAccessToken();
				System.out.println("token: " + token);
				GraphRequest request = GraphRequest.newMeRequest(
					token,
					new GraphRequest.GraphJSONObjectCallback() {
						@Override
						public void onCompleted(JSONObject object, GraphResponse response) {
							sm.save("status", true);
							try {
								String[] accountDetails = new String[2];
								JSONObject jObj = response.getJSONObject();
								System.out.println(jObj.getString("id"));
								System.out.println(jObj.getString("name"));
								String pic  = jObj.getJSONObject("picture").getJSONObject("data").getString("url");
								System.out.println(pic);
								accountDetails[0] = jObj.getString("id");
								accountDetails[1] = jObj.getString("name");
								accountDetails[2] = jObj.getJSONObject("picture").getJSONObject("data").getString("url");
								fbtask.execute(jObj.toString());
								System.out.println("after execute");
							}
							catch(Exception e) {
								e.printStackTrace();
							}
						}
					});
					Bundle parameters = new Bundle();
					parameters.putString("fields", "id, name, picture");
					request.setParameters(parameters);
					request.executeAsync();
			}

			@Override
			public void onCancel() {
				System.out.println("LOGIN CANCELLED");
			}

			@Override
			public void onError(FacebookException e) {
				System.out.println("FACBOOK ERROR: " +  e);
			}
		});
	}

	public void initWidgets() {
		joButton = (Button)findViewById(R.id.login);
		joButton.setEnabled(false);
		iv = (ImageView)findViewById(R.id.logo);
		errorMessage = (TextView)findViewById(R.id.error);
		errorMessages.add(errorMessage);
		email = (EditText)findViewById(R.id.email);
		password = (EditText)findViewById(R.id.password);
		fields.add(email);
		fields.add(password);
	}

	public void signUp(View v) {
		intent = new Intent(this, RegisterActivity.class);
		startActivity(intent);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		callbackManager.onActivityResult(requestCode, resultCode, data);
		System.out.println("resultCode " + resultCode);
		if(resultCode == -1) {
			data = new Intent(LoginActivity.this, ProfileActivity.class);
			startActivity(data);
		}
	}

	public void enableLogin() {
		joButton.setEnabled(true);
	}
}
