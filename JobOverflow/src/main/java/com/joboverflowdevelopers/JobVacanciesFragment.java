package com.joboverflowdevelopers;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.content.Intent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import android.support.v4.app.Fragment;
import android.view.MenuInflater;
import android.content.Context;

public class JobVacanciesFragment extends Fragment {
	private View view;
	private SearchView searchView;
	private RecyclerView listshowjob;
	private List<Job> joblists;
	private JobAdapter adapter;
	private Context ctx;
	private static JobVacanciesFragment _instance = null;

	public static JobVacanciesFragment instance() {
		if(_instance == null) {
			_instance = new JobVacanciesFragment();
		}
		return(_instance);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.job_vacancies, container, false);
		ctx = getActivity();
		setHasOptionsMenu(true);
		joblists = new ArrayList<>();
		joblists.add(new Job("Looking for Full-time Java Developer", "Must have experience in programming", "Php: 40,000", R.drawable.logomain));
		joblists.add(new Job("Freelancer/Part-time Job for Programmers", "Contact me @ email", "Php: 20,000", R.drawable.logomain));
		joblists.add(new Job("Designer for our thesis", "Must know how to use ReactJS", "Php: 25,000", R.drawable.logomain));
		joblists.add(new Job("Looking for our new Accounting Staff", "Hi, please check the following requirements for this job below.", "Php: 18,000", R.drawable.logomain));
		joblists.add(new Job("English Tutor for Korean Students","Willing to work nightshift, must know how to speak English and Korean", "Php: 22,000", R.drawable.logomain));
		joblists.add(new Job("Urgent hiring! Production operators near Sta.Rosa", "Please submit the requirements below.", "Php: 12,000", R.drawable.logomain));
		joblists.add(new Job("Looking for Virtual assistant who can assist me", "Contact me @ my email for more details", "Php: 27,000", R.drawable.logomain));
		listshowjob = (RecyclerView) view.findViewById(R.id.listshow);
		listshowjob.setHasFixedSize(true);
		LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ctx);
		listshowjob.setLayoutManager(linearLayoutManager);
		adapter = new JobAdapter(joblists, ctx);
		listshowjob.setAdapter(adapter);
		return view;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.search_menu, menu);
		final MenuItem myActionMenuJob = menu.findItem(R.id.search);
		searchView = (SearchView) myActionMenuJob.getActionView();
		((EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text)).setHintTextColor(getResources().getColor(R.color.white));
		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String query) {
				if (!searchView.isIconified()) {
					searchView.setIconified(true);
				}
				myActionMenuJob.collapseActionView();
				return false;
			}

			@Override
			public boolean onQueryTextChange(String newText) {
				final  List<Job> filterJobList = filter(joblists,newText);
				adapter.setfilter(filterJobList);
				return true;
			}
		});
	}

	private List<Job> filter(List<Job> jl,String query)
	{
		query = query.toLowerCase();
		final List<Job> filteredJobList = new ArrayList<>();
		for(Job jb : jl)
		{
			final String text = jb.getName().toLowerCase();
			if (text.contains(query))
			{
				filteredJobList.add(jb);
			}
		}
		return filteredJobList;
	}
}
