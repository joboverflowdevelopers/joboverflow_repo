package com.joboverflowdevelopers;

import android.widget.BaseExpandableListAdapter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.widget.TextView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.database.DataSetObserver;

public class ExpandableListAdapterCustom extends BaseExpandableListAdapter
{
	private List<String> listTitle;
	private HashMap<String, List<String>> expandableChildList;
	private Context ctx;

	public ExpandableListAdapterCustom (Context ctx, List<String> listTitle, 
		HashMap<String, List<String>> expandableChildList) {
		this.ctx = ctx;
		this.listTitle = listTitle;
		this.expandableChildList = expandableChildList;
	}

	@Override
	public int getGroupCount() {
		return listTitle.size();
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return expandableChildList.get(listTitle.get(groupPosition)).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return listTitle.get(groupPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		return expandableChildList.get(listTitle.get(groupPosition)) .get(childPosition);
	}
 
	@Override
	public View getGroupView(int groupPosition, boolean isExpanded, 
		View convertView, ViewGroup parent) {
			String item = (String) getGroup(groupPosition);
			convertView = LayoutInflater.from(ctx).inflate(R.layout.expandable_group, null);
			TextView txtvGroup = (TextView) convertView.findViewById(R.id.listTitle);
			txtvGroup.setText(item);
			return convertView;
		}

	@Override
	public View getChildView(int groupPosition, int childPosition, 
		boolean isLastChild, View convertView, ViewGroup parent) {
			String titleCategories = (String) getChild(groupPosition, childPosition);
			convertView = LayoutInflater.from(ctx).inflate(R.layout.expandable_child_group, null);
			TextView txtvGroup = (TextView) convertView.findViewById(R.id.expandedListItem);
			txtvGroup.setText(titleCategories);
			return convertView;
		}

	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public boolean areAllItemsEnabled() {
		return false;
	}

	@Override
	public long getCombinedChildId(long groupId, long childId) {
		return 0;
	}

	@Override
	public long getCombinedGroupId(long groupId) {
		return 0;
	}
	
	@Override
	public boolean isEmpty() {
		return false;
	}

	@Override
	public int getChildTypeCount() {
		return expandableChildList.size();
	}

	@Override
	public void onGroupCollapsed(int groupPosition) {}

	@Override
	public void onGroupExpanded(int groupPosition) {}
	@Override

	public void registerDataSetObserver(DataSetObserver observer){}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {}

	@Override
	public int getChildType(int groupPosition, int childPosition) {return 0;}

	@Override
	public int getGroupType(int groupPosition) { return 0;}

	@Override
	public int getGroupTypeCount() {return 1;}
}
