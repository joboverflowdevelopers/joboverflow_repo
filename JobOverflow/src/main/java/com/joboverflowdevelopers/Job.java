package com.joboverflowdevelopers;

public class Job {
	private String name;
	private String description;
	private String price;
	private int photo;
	
	public Job(String name, String description, String price, int photo) {
		this.name = name;
		this.description = description;
		this.price = price;
		this.photo = photo;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public int getPhoto() {
		return photo;
	}

	public void setPhoto(int photo) {
		this.photo = photo;
	}
}
