package com.joboverflowdevelopers;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Filterable;
import android.widget.Filter;
import java.util.ArrayList;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.TransactionHolder> implements Filterable {
	private ArrayList<Transactions> transactionList;
	private ArrayList<Transactions> transactionListFull;
	private OnItemClickListener listener;
		public interface OnItemClickListener {
		void onItemClick(int position);
	};

	public void setOnItemClickListener(OnItemClickListener l) {
		listener = l;
	};

	public static class TransactionHolder extends RecyclerView.ViewHolder {
		public ImageView mImageView;
		public TextView mTextView1;
		public TextView mTextView2;
		public TextView mTextView3;
		public TransactionHolder(View transactionView, final OnItemClickListener listen) {
			super(transactionView);
			mImageView = transactionView.findViewById(R.id.imageView);
			mTextView1 = transactionView.findViewById(R.id.textView);
			mTextView2 = transactionView.findViewById(R.id.textView2);
			mTextView3 = transactionView.findViewById(R.id.textView3);
			transactionView.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					if(listen != null) {
						int position = getAdapterPosition();
						if(position != RecyclerView.NO_POSITION) {
							listen.onItemClick(position);
						}
					}
				}
			});
		}
	}

	public CustomAdapter(ArrayList<Transactions> transactionList) {
		this.transactionList = transactionList;
		transactionListFull = new ArrayList<>(transactionList);
	}

	@Override
	public TransactionHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.transactions, parent, false);
		TransactionHolder trh = new TransactionHolder(v, listener);
		return(trh);
	}

	@Override
	public void onBindViewHolder(TransactionHolder holder, int position) {
		Transactions transactios = transactionList.get(position);
		holder.mImageView.setImageResource(transactios.getImageResource());
		holder.mTextView1.setText(transactios.getText1());
		holder.mTextView2.setText(transactios.getText2());
		holder.mTextView3.setText(transactios.getText3());
	}

	@Override
	public int getItemCount() {
		return transactionList.size();
	}

	@Override
	public Filter getFilter() {
		return(transactionFilter);
	}

	private Filter transactionFilter = new Filter() {
		@Override
		protected FilterResults performFiltering(CharSequence constraint) {
			ArrayList<Transactions> filteredList = new ArrayList<>();
			if(constraint == null || constraint.length() == 0) {
				filteredList.addAll(transactionListFull);
			}
			else {
				String filterPattern = constraint.toString().toLowerCase().trim();
				for(Transactions trans : transactionListFull) {
					if(trans.getText1().toLowerCase().contains(filterPattern)) {
						filteredList.add(trans);
					}
				}
			}
			FilterResults results = new FilterResults();
			results.values = filteredList;
			return(results);
		}

		@Override
		protected void publishResults(CharSequence constraint, FilterResults results) {
			transactionList.clear();
			transactionList.addAll((ArrayList)results.values);
			notifyDataSetChanged();
		}
	};
}
