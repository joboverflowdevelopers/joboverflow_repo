package com.joboverflowdevelopers;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.content.Intent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.List;
import android.support.v4.app.Fragment;
import android.view.MenuInflater;
import android.content.Context;

public class MessagesFragment extends Fragment {
	private View view;
	private SearchView searchView;
	private RecyclerView showMessages;
	private List<Conversation> convolists = new ArrayList<>();
	private CustomMessages msgAdapter;
	private Context ctx;
	private static MessagesFragment _instance = null;

	public static MessagesFragment instance() {
		if(_instance == null) {
			_instance = new MessagesFragment();
		}
		return(_instance);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle saveInstanceState) {
		view = inflater.inflate(R.layout.activity_messages, container, false);
		ctx = getActivity();
		setHasOptionsMenu(true);
		convolists.add(new Conversation("Edward Isles","Congratulations, you're hired", "Aug 30", R.drawable.convo1));
		convolists.add(new Conversation("Louievic Sancon","We will call you.", "Aug 30", R.drawable.convo2));
		convolists.add(new Conversation("Heji Palanski","Your final interview is scheduled today.", "Aug 30", R.drawable.convo3));
		convolists.add(new Conversation("Bernie Baltazar","Great Conversation. You passed our programming assessment.", "Aug 30",R.drawable.convo4));
		convolists.add(new Conversation("Patrick Obra","Welcome to our company", "Aug 30",R.drawable.convo5));
		showMessages = (RecyclerView) view.findViewById(R.id.listshow);
		showMessages.setHasFixedSize(true);
		LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ctx);
		showMessages.setLayoutManager(linearLayoutManager);
		msgAdapter = new CustomMessages(convolists, ctx);
		showMessages.setAdapter(msgAdapter);
		return view;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.search_menu, menu);
		final MenuItem messagesMenu = menu.findItem(R.id.search);
		searchView = (SearchView) messagesMenu.getActionView();
		((EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text)).setHintTextColor(getResources().getColor(R.color.white));
		searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String query) {
				if (!searchView.isIconified()) {
					searchView.setIconified(true);
				}
				messagesMenu.collapseActionView();
				return(false);
			}

			@Override
			public boolean onQueryTextChange(String newText) {
				final  List<Conversation> messageFilter = filter(convolists,newText);
				msgAdapter.setfilter(messageFilter);
				return(true);
			}
		});
	}

	private List<Conversation> filter(List<Conversation> cnvs,String query) {
		query=query.toLowerCase();
		final List<Conversation> filteredMessage=new ArrayList<>();
		for(Conversation cnv : cnvs) {
			final String text = cnv.getName().toLowerCase();
			if (text.contains(query)) {
				filteredMessage.add(cnv);
			}
		}
		return(filteredMessage);
	}
}
