const https = require('https');
const http = require('http');
const fs = require('fs');
const mongojs = require('mongojs');
const db = mongojs('joboverflowdb');
const nconf = require('nconf');
nconf.argv().env().file('keys.json');
const redis = require('redis');
const client = redis.createClient();
const sha1 = require('sha1');
const handlers = {};
let sessionID = '';
const webclients = [];

handlers['/'] = (req, res) => {
	res.writeHead(200, { "Content-Type" : "application/json" });
		res.end(JSON.stringify({
		loginStatus: 'blocked',
		sid: null
	}));
}



handlers['/register'] = (req, res) => {
	console.log("register");
	if(req.method == 'POST') {
		let data = '';
		req.on('data', (chunk) => {
			data += chunk.toString();
		});
		req.on('end', () => {
			let account = JSON.parse(data);
			account.password = sha1(account.password);
			let email = account.email;
			db.accounts.findOne({email: account.email.toString()}, (err, doc) => {
				switch(doc) {
					case null:
						let forsessionid = account.email + account.password;
						console.log('account: ' + JSON.stringify(account));
						startSession(forsessionid, account, (id) => {
							console.log('new session id: ' + id);
							res.writeHead(200, { "Content-Type" : "application/json" });
							res.end(JSON.stringify({
								reg_stat: 'sucess'
							}));
							console.log('sucessful registration');
						});
						break;
					default:
						console.log('has match');
						res.writeHead(200, { "Content-Type" : "application/json" });
						res.end(JSON.stringify({
							reg_stat: 'exists'
						}));
						console.log('email already in use');
						break;
				}
			});
		});
	}
	else {
		res.writeHead(404, { "Content-Type" : "text/plain" });
		res.end(req.url + " not found");
	}
}

handlers['/login'] = (req, res) => {
	let data = '';
	req.on('data', (chunk) => {
		data += chunk.toString();
	});
	req.on('end', () => {
		let account = JSON.parse(data);
		account.password = sha1(account.password);
		loggedUser = account;
		db.accounts.findOne(account, (err, matchedUser) => {
			if(matchedUser != null) {
				let forsessionid = account.email + account.password;
				 client.get(forsessionid, (err, reply) => {
					console.log('login session: ' + reply);
					loggedUser['session_id'] = reply;
					console.log('does session exists');
					if(reply) {
						db.accounts.findOne({email: loggedUser.email.toString()}, (err, matchedUser) => {
							loggedUser.username = matchedUser.username;
							loggedUser.email = matchedUser.email;
							console.log('SUCCESSFUL LOGIN: sessionIDFromRedis: ' + loggedUser.session_id);
							res.writeHead(200, { "Content-Type" : "application/json" });
							res.end(JSON.stringify({
								loginStatus: 'granted',
								session_id: account.session_id
							}));
							console.log('granted sid: ' + account.session_id);
						});
					}
					else {
						reStartSession(forsessionid, account, (id) => {
							console.log('new session id: ' + id);
							db.account.update({email: loggedUser.email}, {$set:{session_id: id}}, () => {
								loggedUser['session_id'] = id;
								res.writeHead(200, { "Content-Type" : "application/json" });
								res.end(JSON.stringify ({
									loginStatus: 'granted',
									session_id: id
								}));
							});
						});
					}
				});
			}
			else {
				res.writeHead(200, { "Content-Type" : "application/json" });
				res.end(JSON.stringify({
					loginStatus: 'blocked'
				}));
				console.log('log in failed');
			}
		});
	});
}

let insertNewAccount = (account) => {
	db.accounts.insert(account);
	console.log('account sucessfully added to database');
}

const port = 3434;

let httpsServerOptions = {
	'key' : fs.readFileSync('./https/key.pem'),
	'cert' : fs.readFileSync('./https/cert.pem')
};

let httpsServer = https.createServer(httpsServerOptions, (req, res) => {
	console.log(req.url);
	if(handlers[req.url]) {
		handlers[req.url](req, res);
	}
	else if(req.url.includes("?session_id")) {
		let url = req.url.split("?");
		handlers[url[0]](req, res);
	}
	else if(req.url.includes("auth")) {
		let qs = querystring.parse(req.url);
		oauthToken = qs['/auth/twitter/callback?oauth_token'];
		verifier = qs['oauth_verifier'];
		if(verifier != undefined) {
			res.writeHead(301, {"Location" : "https://192.168.254.140:3434/auth/twitter/callback"});
			res.end();
		}
	}
	else {
		file.serve(req, res, (err, result) => {
			if(err) {
				res.writeHead(404, { "Content-Type" : "text/plain" });
				res.end(req.url + " not found");
			}
		});
	}
});

httpsServer.listen(port);
console.log('go to https://192.168.101.2:3434');

function startSession(name, account, callback) {
	id = sha1(name);
	account.session_id = id;
	insertNewAccount(account);
	sessionID = id;
	client.set(name, id, () => {
		callback(id);
	});
}

function reStartSession(name, account, callback) {
	id = sha1(name);
	db.accounts.update({email: account.email}, {$set:{session_id: id}});
	sessionID = id;
	client.set(name, id, () => {
		callback(id);
	});
}

function doesSessionExists(sessionid, callback) {
	client.exists(sessionid, (err, reply) => {
		callback(reply);
	});
}

function getSession(key) {
	let sessionid = '';
	client.get(key, (err, reply) => {
		console.log('login session: ' + reply);
		sessionID = reply;
		return(reply);
	});
}

function endSession(sessionid, callback) {
	client.del(sessionid, (err, reply) => {
		callback();
	});
}
